# Install transformers library if not already installed
!pip install transformers

import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import ModelCheckpoint
import transformers
from keras.models import load_model  # Import load_model function from Keras

# Define a class CARS24BERT
class CARS24BERT:

  def __init__(self):
    pass

  def bert_encode(self, texts, tokenizer, max_len=128):
    """
    Tokenizes input texts using BERT tokenizer.
    """
    all_tokens = []

    for text in texts:
      text = tokenizer.tokenize(text)  # Tokenize text
      text = text[:max_len-2]  # Truncate tokens to fit max_len - 2
      input_sequence = ["[CLS]"] + text + ["[SEP]"]  # Add special tokens [CLS] and [SEP]
      pad_len = max_len - len(input_sequence)  # Calculate padding length

      tokens = tokenizer.convert_tokens_to_ids(input_sequence)  # Convert tokens to IDs
      tokens += [0] * pad_len  # Pad sequence with 0s
      pad_masks = [1] * len(input_sequence) + [0] * pad_len  # Create attention masks
      segment_ids = [0] * max_len  # Create segment IDs (not used in this case)

      all_tokens.append(tokens)  # Append tokens to all_tokens list

    return np.array(all_tokens)  # Return tokens as numpy array

  def build_model(self, transformer, max_len=128):
    """
    Build a BERT-based classification model.
    """
    input_word_ids = Input(shape=(max_len,), dtype=tf.int32, name="input_word_ids")  # Define input layer
    sequence_output = transformer(input_word_ids)[0]  # BERT output
    cls_token = sequence_output[:, 0, :]  # Extract [CLS] token
    out = Dense(1, activation='sigmoid')(cls_token)  # Output layer with sigmoid activation for binary classification

    model = Model(inputs=input_word_ids, outputs=out)  # Define the model
    model.compile(Adam(lr=1e-5), loss='binary_crossentropy', metrics=['accuracy'])  # Compile the model

    return model  # Return the compiled model

  def train_model(self, train_data, test_data, path_to_save_checkpoints=None):
    """
    Train the BERT-based classification model.
    """
    training_data = pd.read_csv(train_data).sample(frac=1)  # Read and shuffle training data
    testing_data = pd.read_csv(test_data).sample(frac=1).dropna()  # Read, shuffle, and drop NA rows from testing data

    transformer_layer = transformers.TFDistilBertModel.from_pretrained('distilbert-base-uncased')  # Load BERT transformer
    tokenizer = transformers.DistilBertTokenizer.from_pretrained('distilbert-base-uncased')  # Load BERT tokenizer

    model = self.build_model(transformer_layer, max_len=128)  # Build the BERT-based model
    train_input = self.bert_encode(training_data.Trans.values, tokenizer, max_len=128)  # Tokenize and encode training data
    test_input = self.bert_encode(testing_data.TRANS.values, tokenizer, max_len=128)  # Tokenize and encode testing data
    train_labels = training_data.Label.values  # Get training labels

    model.fit(
        train_input, train_labels,
        validation_split=0.2,
        epochs=3,
        batch_size=4
    )  # Train the model

    model_save_path = f"/content/drive/MyDrive/Capstone_project/Cars24Model_{format(time.strftime('log_%Y_%m_%d_%H_%M_%S'))}.h5"
    model.save(model_save_path)  # Save the trained model

    return model  # Return the trained model

  def make_prediction(self, path_to_saved_checkpoints, prediction_value):
    """
    Make predictions using the trained BERT-based classification model.
    """
    new_transformer_layer = transformers.TFDistilBertModel.from_pretrained('distilbert-base-uncased')  # Load BERT transformer
    prediction_model = self.build_model(new_transformer_layer, max_len=128)  # Build prediction model
    prediction_model.load_weights(path_to_saved_checkpoints)  # Load weights into prediction model
    tokenizer = transformers.DistilBertTokenizer.from_pretrained('distilbert-base-uncased')  # Load BERT tokenizer

    text_to_predict = np.array(prediction_value)  # Convert prediction value to numpy array
    pred = self.bert_encode(text_to_predict, tokenizer, max_len=128)  # Tokenize and encode prediction text

    score = prediction_model.predict(pred, verbose=1)  # Predict using the prediction model

    return score  # Return prediction scores

import time  # Import time module

MODEL = CARS24BERT()  # Create an instance of CARS24BERT class

# Concatenate and shuffle data for training and testing
m_data = pd.concat([df, df1]).sample(frac=1)
m_data = m_data.sample(frac=1)

train = m_data[:3600]  # Split data into training and testing sets
test = m_data[3600:]

train['Label'].value_counts()  # Display counts of unique labels in training set
test['Label'].value_counts()  # Display counts of unique labels in testing set

train.to_csv('train.csv', index=False)  # Save training data to CSV file
test.to_csv('test.csv', index=False)  # Save testing data to CSV file

# Train the BERT-based classification model
MODEL.train_model('/content/train.csv', '/content/test.csv')

# Make predictions using the trained model
MODEL.make_prediction('path/to/saved/checkpoints', 'your_prediction_text_here')
