#Code is same as training one all functions are same we are just passing the trained model here

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchaudio
from transformers import AutoConfig, Wav2Vec2FeatureExtractor
import librosa
import numpy as np
import pandas as pd

from dataclasses import dataclass
from typing import Optional, Tuple
import torch
from transformers.file_utils import ModelOutput
import shutil
import torch.nn as nn
from torch.nn import BCEWithLogitsLoss, CrossEntropyLoss, MSELoss
from IPython.display import Markdown, display
from transformers.models.wav2vec2.modeling_wav2vec2 import (
	Wav2Vec2PreTrainedModel,
	Wav2Vec2Model
)
import pickle
import json
import glob
import time

from pydub import AudioSegment
from pydub.utils import make_chunks
import glob

import os

from pymongo import MongoClient
import os
import csv
import pandas as pd
import dateutil
from collections import Counter
from datetime import datetime
import glob
import re
import pandas as pd
import glob
import time
import csv
from pydub import AudioSegment
from pydub import AudioSegment
from glob import glob
import subprocess
import os

client=MongoClient('167.71.224.78',27017)
db=client['bajaj']
collection=db['append_dset']

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model_name_or_path = "m3hrdadfi/wav2vec2-xlsr-persian-speech-emotion-recognition"

@dataclass
class SpeechClassifierOutput(ModelOutput):
	loss: Optional[torch.FloatTensor] = None
	logits: torch.FloatTensor = None
	hidden_states: Optional[Tuple[torch.FloatTensor]] = None
	attentions: Optional[Tuple[torch.FloatTensor]] = None


class Wav2Vec2ClassificationHead(nn.Module):
	"""Head for wav2vec classification task."""

	def __init__(self, config):
		super().__init__()
		self.dense = nn.Linear(config.hidden_size, config.hidden_size)
		self.dropout = nn.Dropout(config.final_dropout)
		self.out_proj = nn.Linear(config.hidden_size, config.num_labels)

	def forward(self, features, **kwargs):
		x = features
		x = self.dropout(x)
		x = self.dense(x)
		x = torch.tanh(x)
		x = self.dropout(x)
		x = self.out_proj(x)
		return x

class Wav2Vec2ForSpeechClassification(Wav2Vec2PreTrainedModel):
	def __init__(self, config):
		super().__init__(config)
		self.num_labels = config.num_labels
		self.pooling_mode = config.pooling_mode
		self.config = config

		self.wav2vec2 = Wav2Vec2Model(config)
		self.classifier = Wav2Vec2ClassificationHead(config)

		self.init_weights()

	def freeze_feature_extractor(self):
		self.wav2vec2.feature_extractor._freeze_parameters()

	def merged_strategy(
		self,
		hidden_states,
		mode="mean"
	):
		if mode == "mean":
			outputs = torch.mean(hidden_states, dim=1)
		elif mode == "sum":
			outputs = torch.sum(hidden_states, dim=1)
		elif mode == "max":
			outputs = torch.max(hidden_states, dim=1)[0]
		else:
			raise Exception(
				"The pooling method hasn't been defined! Your pooling mode must be one of these ['mean', 'sum', 'max']")

		return outputs

	def forward(
			self,
			input_values,
			attention_mask=None,
			output_attentions=None,
			output_hidden_states=None,
			return_dict=None,
			labels=None,
	):
		return_dict = return_dict if return_dict is not None else self.config.use_return_dict
		outputs = self.wav2vec2(
			input_values,
			attention_mask=attention_mask,
			output_attentions=output_attentions,
			output_hidden_states=output_hidden_states,
			return_dict=return_dict,
		)
		hidden_states = outputs[0]
		hidden_states = self.merged_strategy(hidden_states, mode=self.pooling_mode)
		logits = self.classifier(hidden_states)

		loss = None
		if labels is not None:
			if self.config.problem_type is None:
				if self.num_labels == 1:
					self.config.problem_type = "regression"
				elif self.num_labels > 1 and (labels.dtype == torch.long or labels.dtype == torch.int):
					self.config.problem_type = "single_label_classification"
				else:
					self.config.problem_type = "multi_label_classification"

			if self.config.problem_type == "regression":
				loss_fct = MSELoss()
				loss = loss_fct(logits.view(-1, self.num_labels), labels)
			elif self.config.problem_type == "single_label_classification":
				loss_fct = CrossEntropyLoss()
				loss = loss_fct(logits.view(-1, self.num_labels), labels.view(-1))
			elif self.config.problem_type == "multi_label_classification":
				loss_fct = BCEWithLogitsLoss()
				loss = loss_fct(logits, labels)

		if not return_dict:
			output = (logits,) + outputs[2:]
			return ((loss,) + output) if loss is not None else output

		return SpeechClassifierOutput(
			loss=loss,
			logits=logits,
			hidden_states=outputs.hidden_states,
			attentions=outputs.attentions,
			)

def speech_file_to_array_fn(path, sampling_rate):
	speech_array, _sampling_rate = torchaudio.load(path)
	resampler = torchaudio.transforms.Resample(_sampling_rate)
	speech = resampler(speech_array).squeeze().numpy()
	return speech

def predict(path, sampling_rate):
	speech = speech_file_to_array_fn(path, sampling_rate)
	inputs = feature_extractor(speech, sampling_rate=sampling_rate, return_tensors="pt", padding=True)
	inputs = {key: inputs[key].to(device) for key in inputs}
	with torch.no_grad():
		logits = model(**inputs).logits
	scores = F.softmax(logits, dim=1).detach().cpu().numpy()[0]
	outputs = [{"Emotion": config.id2label[i], "Score": f"{round(score * 100, 3):.1f}"} for i, score in enumerate(scores)]
	return outputs

config = AutoConfig.from_pretrained(model_name_or_path)
feature_extractor = Wav2Vec2FeatureExtractor.from_pretrained(model_name_or_path)

sampling_rate = feature_extractor.sampling_rate
model = Wav2Vec2ForSpeechClassification.from_pretrained('/home/satyam/Downloads/best-20230212T154950Z-001/best').to(device)

def split_into_mono(file, output_dir):

	name = file.split('/')[-1]
	out_l = output_dir + name
	out_r = output_dir + 'r_' + name
	sox_l = ['sox',file, out_l, 'remix', str(1)]
	subprocess.run(sox_l)
	return out_l
import time

def Transformer_prediction_with_stime_ttime(unique_value,path_to_audio):
	counter=0
	current_dir = os.getcwd()
	folders = ['PathToSaveChunk','PathToSaveMono','PathToSaveSmallChunk']
	for folder in folders:
		isExist = os.path.exists(folder)
		if not isExist:
			os.mkdir(os.path.join(current_dir,folder))

	for cursor in collection.find({'file_name':unique_value}):
		codetime = time.time()
		for arrow in cursor['paragraphs']:
			if arrow['speaker']==1:
				if float(arrow['start_time']) < 0.1:
					startTime = arrow['start_time'] * 1000
				else:
					startTime = (float(arrow['start_time'])-0.03)*1000

				endTime = (float(arrow['till_time'])+0.03)*1000
				song = AudioSegment.from_file(path_to_audio+cursor['file_name']+'.wav')
				# song, sr = librosa.load('/home/satyam/Documents/neww/Abhishek.Aspat__NayanPSFOutboundCampaign__16__-1__6005650262__2022-09-26_13-11-38.wav', sr=16000)
				# print(song.get_array_of_samples())
				extract = song[startTime:endTime]
				extract.export(current_dir+'/'+'PathToSaveChunk'+'/'+cursor['file_name']+'_'+str(counter)+'.wav', format="wav")
				split_into_mono(current_dir+'/'+'PathToSaveChunk'+'/'+cursor['file_name']+'_'+str(counter)+'.wav',current_dir+'/'+'PathToSaveMono/')

				myaudio = AudioSegment.from_file(current_dir+'/PathToSaveMono/'+cursor['file_name']+'_'+str(counter)+'.wav', "wav")
				# '/home/satyam/Documents/neww/PathToSaveChunk/Abhishek.Aspat__NayanPSFOutboundCampaign__16__-1__9886129475__2022-09-28_17-42-50_0.wav'
				# '/home/satyam/Documents/neww/PathToSaveMono/Abhishek.Aspat__NayanPSFOutboundCampaign__16__-1__9886129475__2022-09-28_17-42-50_0.wav'
				chunk_length_ms = 6000
				chunks = make_chunks(myaudio, chunk_length_ms)
				for i, chunk in enumerate(chunks):
					chunk_name = "chunk{0}.wav".format(i)
					# print("exporting", chunk_name)
					chunk.export(current_dir+'/'+'PathToSaveSmallChunk/'+chunk_name, format="wav")
					outputs = predict(current_dir+'/'+'PathToSaveSmallChunk/'+chunk_name, sampling_rate)
					if float(outputs[0]['Score'])>=90:
						print('Fumbling')
						print('\n')
						print(arrow['index'])
					if float(outputs[1]['Score'])>=89:
						print('Angry')
						print('\n')
						print(arrow['index'])
					if float(outputs[2]['Score'])>=90:
						print('Normal')
					try:
						os.remove(current_dir+'/'+'PathToSaveChunk/'+cursor['file_name']+'_'+str(counter)+'.wav')
						os.remove(current_dir+'/'+'PathToSaveMono/'+cursor['file_name']+'_'+str(counter)+'.wav')
						os.remove(current_dir+'/'+'PathToSaveSmallChunk/'+chunk_name)
					except:
						pass
					counter+=1
					print("Time Taken--",time.time() - codetime)
	os.system('rm -rf {}'.format(current_dir+'/'+'PathToSaveChunk'))
	os.system('rm -rf {}'.format(current_dir+'/'+'PathToSaveMono'))
	os.system('rm -rf {}'.format(current_dir+'/'+'PathToSaveSmallChunk'))

Transformer_prediction_with_stime_ttime('geetanjali.yadav__NayanPSFOutboundCampaign__16__-1__9792418250__2022-09-26_11-27-42','/home/satyam/Documents/neww/')